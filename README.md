## Synopsis
The Open Data Catalog    
Artuur Bruwier
2MMP ProDuce 
Opleiding Arteveldehogeschool - Grafische en digitale media  - Multimediaproductie
Schooljaar 2015-2016  
[WHADOO](http://www.site.be)

!!!!!! BELANGRIJK !!!!!!
Om de app effectief te testen moet je je uiteraard in (centrum) Gent bevinden. Daarom zijn gewone coördinaten ingesteld in Gent. Mocht je willen testen met GEO-locatie, dan dien je in **whadoo-search.js** lijn **24 & 25** in commentaar te zetten, en bij lijn **20 & 21** de commentaartekens weg te halen.

### Home

Hier krijgt de gebruiker een scherm te zien met het logo van WHADOO, en kan hij ofwel scrollen of door middel van de menunavigatie een optie kiezen. De opties zijn: zoeken, extra & contact.

### Zoeken

Dit is waarrond de app draait. De gebruiker ziet een map en kan op een hamburgermenu rechtsbovenaan klikken. Bij het klikken ontvouwt zich een menu waarin de gebruiker enkele filters kan instellen. Op basis van de keuze van filters wordt informatie (GEOJSONS) op de kaart getoond. 

### Extra

Bij deze tab ziet de gebruiker 2 tiles. Wanneer hij klikt op één van de twee komt hij terecht op een pop-up scherm met de gevraagde informatie. De twee tiles verwijzen respectievelijk naar: 
1. de weersverwachting in Gent voor het komende uur
2. een korte quiz

### Contact

De gebruiker kan contact opnemen met de developer (mezelf in dit geval) via een contactformulier.

### Footer

In de footer is er enerzijds de mogelijkheid om in te schrijven om een maandelijkse nieuwsbrief en anderzijds zijn er ook social media-iconen. 

***
## Links
* [dossier.md](dossier.md)
* [dossier.pdf](dossier.pdf)
* [poster.pdf](poster.pdf)
* [presentatie.pdf](presentatie.pdf)
* [screencast.mpeg](screencast.mpeg)
* [screenshot_320.png](screenshot_320.png)
* [screenshot_480.png](screenshot_480.png)
* [screenshot_640.png](screenshot_640.png)
* [screenshot_800.png](screenshot_800.png)
* [screenshot_960.png](screenshot_960.png)
* [screenshot_1024.png](screenshot_1024.png)
* [screenshot_1280.png](screenshot_1280.png)
* [timesheet](timesheet.xlsx)

***

