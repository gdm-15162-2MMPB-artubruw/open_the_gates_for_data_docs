Dossier
=======================================
#### Open the gates for Data!
The Open Data Catalog  
WHADOO  
Artuur Bruwier  
2MMP ProDuce 
Opleiding Arteveldehogeschool - Grafische en digitale media  - Multimediaproductie  
Schooljaar 2015-2016  
[WHADOO](http://www.site.be)  

!!!!!! BELANGRIJK !!!!!!
Om de app effectief te testen moet je je uiteraard in (centrum) Gent bevinden. Daarom zijn gewone coördinaten ingesteld in Gent. Mocht je willen testen met GEO-locatie, dan dien je in **whadoo-search.js** lijn **24 & 25** in commentaar te zetten, en bij lijn **20 & 21** de commentaartekens weg te halen.

***

## Briefing & analyse

Maak een responsive mobile-first webapplicatie waarin minimaal 6 datasets, afkomstig uit de dataset-pool van Stad Gent, verwerkt zijn. Conceptueel denken is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren van deze datasets, er moet een concept rond gebouwd worden.

Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

WHADOO, een webapplicatie die gebruik maakt van de datasets van Stad Gent, is een applicatie uitermate geschikt voor de student in Gent. Via GEO-locatie wordt zijn/haar positie getoond op een kaart. Daarna kan de student een maximum aantal meter invoeren die hij/zij wil lopen. Verder zijn er ook filters die kunnen worden aan- of uitgevinkt. Op basis van voorgaande keuzes worden alle opties (van bioscopen tot parken tot winkellussen) op de map getoond, die zich binnen de opgestelde radius bevinden. Zo weet de student in geen tijd waar hij/zij naar toe kan gaan. 

Omdat het een app is die vooral studenten beoogt, zijn er ook extra’s voorzien. Dit extra item is opnieuw enorm handig voor de student in Gent. Hij kan namelijk de weersverwachting voor 1u bekijken. Dat is iets die je als student graag snel en duidelijk wilt zien. Bovendien is het ook relevant, gezien de informatie die ze met de WHADOO-search functie krijgen. Ziet men bijvoorbeeld dat men in de buurt is van een park, maar het weer valt niet mee, dan kan de student even het weer voor komend uur bekijken. Wie weet klaart het op! 

Ik koos bewust om frisse kleuren te gebruiken die het doelpubliek wel aanspreken. Verder wou ik de app vooral vrij eenvoudig houden. Dat heb ik gedaan door een One Page App te maken. Bijna alle informatie is te zien op 1 pagina. Gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid staan centraal. De app is volledig responsive en mobile first.  


***

## Functionele specificaties
*	De gebruiker krijgt een scherm te zien met het logo en een hamburgermenu. Hij kan kiezen 		tussen ‘whadoo’, ‘extra’ of ‘contact’.
* 	Bij ‘whadoo’ ziet de gebruiker zijn/haar huidige locatie op een geïntegreerde Google Maps in 		de app. De gebruiker kan door middel van een schuifbalk een maximum aantal meter ingeven
	die hij/zij wilt wandelen, en daarnaast zijn er ook enkele filters die kunnen worden aan- of 		uitgevinkt.
* 	Op basis van de keuze van de gebruiker worden alle bioscopen, parken, speelterreinen, sport		centra en winkellussen getoond die zich binnen de opgegeven straal bevinden.
* 	Daarnaast zijn er ook extra’s. Zo kan de gebruiker de weersverwachting voor komend uur raad		plegen, hetgeen nuttig is om te beslissen of men al dan niet naar een park/bioscoop/... zou 		gaan of niet. Er is ook een korte quiz.
* 	Er is een contactformulier, waarbij de gebruiker contact kan leggen met de developer (mezelf 		dus, in dit geval).
* 	Er is de mogelijkheid tot inschrijven op een maandelijkse nieuwsbrief.
*	De gebruiker kan WHADOO volgen via Facebook of Twitter (buttons in de app).

***

## Niet-functionele specificaties
*	Responsive
*	Mobile first
*	One Page Webapplication
*	Preloaders
*	Google Maps integratie (custom icons)
*	GEO-location
* 	Social Media Bookmarking
*	Interactieve webapplicatie
*	Bookmarking

***

## Technische specificaties
* Core technologies: HTML5, CSS3 en JavaScript
* Storage: JSON bestanden, localstorage

## Ideeënborden
* [Ideeënbord](images/ideeenbord.jpg)

***

## Wireframe 'emailsjabloon'
* [Wireframe 'emailsjabloon' (DESKTOP)](images/wireframes_contact_desktop.jpg)
* [Wireframe 'emailsjabloon' (MOBILE)](images/wireframes_contact_mobile.jpg)

***

## Wireflow van de webapp
* [Wireframe 'home' (DESKTOP)](images/wireframes_home_desktop.jpg)
* [Wireframe 'home' (MOBILE)](images/wireframes_home_mobile.jpg)
* [Wireframe 'whadoo search' (DESKTOP)](images/wireframes_whadoo_desktop.jpg)
* [Wireframe 'whadoo search 1' (MOBILE)](images/wireframes_whadoo_mobile.jpg)
* [Wireframe 'whadoo search 2' (MOBILE)](images/wireframes_whadoo_filters_mobile.jpg)
* [Wireframe 'extra' (DESKTOP)](images/wireframes_extra_desktop.jpg)
* [Wireframe 'extra detail' (DESKTOP)](images/wireframes_extra_detail_desktop.jpg)
* [Wireframe 'extra' (MOBILE)](images/wireframes_extra_mobile.jpg)
* [Wireframe 'extra detail 1' (MOBILE)](images/wireframes_extra_detail_mobile.jpg)
* [Wireframe 'extra detail 2' (MOBILE)](images/wireframes_extra_detail2_mobile.jpg)
* [Wireframe 'contact' (DESKTOP)](images/wireframes_contact_desktop.jpg)
* [Wireframe 'contact' (MOBILE)](images/wireframes_contact_mobile.jpg)
* [Wireframe 'footer' (MOBILE)](images/wireframes_footer_mobile.jpg)

***
## Moodboard
* [Moodboard](images/moodboard.jpg)

***
## Style Tiles
* [Style Tile 1](images/styletile_1.jpg)
* [Style Tile 2](images/styletile_2.jpg)
* [Style Tile 3 (final)](images/styletile_3.jpg)

***

## Visual Designs
* [Visual Design 'home' (MOBILE)](images/visualdesign_home_mobile.jpg)
* [Visual Design 'home menu' (MOBILE)](images/visualdesign_homemenu_mobile.jpg)
* [Visual Design 'whadoo search 1' (MOBILE)](images/visualdesign_whadoo_mobile.jpg)
* [Visual Design 'whadoo search 2' (MOBILE)](images/visualdesign_whadoo_filters_mobile.jpg)
* [Visual Design 'extra' (MOBILE)](images/visualdesign_extra_mobile.jpg)
* [Visual Design 'extra detail 1' (MOBILE)](images/visualdesign_extra_detail_mobile.jpg)
* [Visual Design 'extra detail 2' (MOBILE)](images/visualdesign_extra_detail2_mobile.jpg)
* [Visual Design 'extra detail 3' (MOBILE)](images/visualdesign_extra_detail3_mobile.jpg)
* [Visual Design 'contact' (MOBILE)](images/visualdesign_contact_mobile.jpg)
* [Visual Design 'nieuwsbrief' (MOBILE)](images/visualdesign_nieuwsbrief_mobile.jpg)
* [Visual Design 'footer' (MOBILE)](images/visualdesign_footer_mobile.jpg)

***

## Screenshots 

* [Mobile - global 1](images/screenshots/screenshot_mobile_global1.png)
* [Mobile - global 2](images/screenshots/screenshot_mobile_global2.png)
* [Mobile - detail 1](images/screenshots/screenshot_mobile_detail1.png)
* [Mobile - detail 2](images/screenshots/screenshot_mobile_detail2.png)
* [Desktop - global](images/screenshots/screenshot_desktop_global.png)
* [Desktop - detail 1](images/screenshots/screenshot_desktop_detail1.png)
* [Desktop - detail 2](images/screenshots/screenshot_desktop_detail2.png)

***
## Screencast
[Screencast](screencast.mpg)
***
## Snippets
* [HTML](images/code_snippets/HTML.png)
* [CSS](images/code_snippets/CSS.png)
* [JavaScript 1](images/code_snippets/javascript1.png)
* [JavaScript 2](images/code_snippets/javascript2.png)
* [JavaScript 3](images/code_snippets/javascript3.png)

***

# Tijdsbesteding
* [Tijdsbesteding](timesheet.xlsx)